package calcoperations;

import calcexceptions.DivisionByZeroException;
import calcexceptions.FewStackSizeException;
import calcexceptions.ManyArgumentsException;
import calcexceptions.NumberOverflowException;
import org.junit.jupiter.api.Test;
import stackcalc.StackCalc;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

class DivOperationTest {
    @Test
    void testDivOperation() throws Exception {
        StackCalc testDiv = new StackCalc();
        assertThrows(ManyArgumentsException.class,
                                            () -> testDiv.calculate(new BufferedReader(new StringReader("/ asdqweqwe\nsadqw"))));

        assertThrows(FewStackSizeException.class,
                                            () -> testDiv.calculate(new BufferedReader(new StringReader("/\nPRINT"))));

        testDiv.calculate(new BufferedReader(new StringReader("PUSH 100")));
        assertThrows(FewStackSizeException.class,
                                            () -> testDiv.calculate(new BufferedReader(new StringReader("/\n# qwerty"))));

        testDiv.calculate(new BufferedReader(new StringReader("PUSH 10\n/")));
        assertEquals(10, testDiv.getTopStack());

        assertThrows(DivisionByZeroException.class,
                                            () -> testDiv.calculate(new BufferedReader(new StringReader("PUSH 0\n/"))));

        testDiv.calculate(new BufferedReader(new StringReader("PUSH 1e300")));
        testDiv.calculate(new BufferedReader(new StringReader("PUSH 1e-300")));
        assertThrows(NumberOverflowException.class,
                                            () -> testDiv.calculate(new BufferedReader(new StringReader("/"))));


    }

}