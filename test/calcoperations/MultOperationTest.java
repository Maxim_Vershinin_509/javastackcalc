package calcoperations;

import calcexceptions.FewStackSizeException;
import calcexceptions.ManyArgumentsException;
import calcexceptions.NumberOverflowException;
import org.junit.jupiter.api.Test;
import stackcalc.StackCalc;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

class MultOperationTest {
    @Test
    void testMultOperation() throws Exception {
        StackCalc testMult = new StackCalc();
        assertThrows(ManyArgumentsException.class,
                () -> testMult.calculate(new BufferedReader(new StringReader("* asdqweqwe\nsadqw"))));

        assertThrows(FewStackSizeException.class,
                () -> testMult.calculate(new BufferedReader(new StringReader("*\nPRINT"))));

        testMult.calculate(new BufferedReader(new StringReader("PUSH 100")));
        assertThrows(FewStackSizeException.class,
                () -> testMult.calculate(new BufferedReader(new StringReader("*\n# qwerty"))));

        testMult.calculate(new BufferedReader(new StringReader("PUSH 10\n*")));
        assertEquals(1000, testMult.getTopStack());

        testMult.calculate(new BufferedReader(new StringReader("PUSH 1e300")));
        testMult.calculate(new BufferedReader(new StringReader("PUSH 1e300")));
        assertThrows(NumberOverflowException.class,
                () -> testMult.calculate(new BufferedReader(new StringReader("*"))));


    }
}