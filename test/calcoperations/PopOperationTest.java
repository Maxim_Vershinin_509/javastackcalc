package calcoperations;

import calcexceptions.FewStackSizeException;
import calcexceptions.ManyArgumentsException;
import org.junit.jupiter.api.Test;
import stackcalc.StackCalc;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

class PopOperationTest {
    @Test
    void testPopOperation() throws Exception {
        StackCalc testPop = new StackCalc();

        assertThrows(ManyArgumentsException.class,
                                            () -> testPop.calculate(new BufferedReader(new StringReader("POP Many"))));

        assertThrows(FewStackSizeException.class,
                                            () -> testPop.calculate(new BufferedReader(new StringReader("POP"))));
    }

}