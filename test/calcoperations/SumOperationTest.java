package calcoperations;

import calcexceptions.FewStackSizeException;
import calcexceptions.ManyArgumentsException;
import calcexceptions.NumberOverflowException;
import org.junit.jupiter.api.Test;
import stackcalc.StackCalc;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

class SumOperationTest {
    @Test
    void testSumOperation() throws Exception {
        StackCalc testSum = new StackCalc();
        assertThrows(ManyArgumentsException.class,
                () -> testSum.calculate(new BufferedReader(new StringReader("+ asdqweqwe\nsadqw"))));

        assertThrows(FewStackSizeException.class,
                () -> testSum.calculate(new BufferedReader(new StringReader("+\nPRINT"))));

        testSum.calculate(new BufferedReader(new StringReader("PUSH 100")));
        assertThrows(FewStackSizeException.class,
                () -> testSum.calculate(new BufferedReader(new StringReader("+\n# qwerty"))));

        testSum.calculate(new BufferedReader(new StringReader("PUSH 10\n+")));
        assertEquals(110, testSum.getTopStack());
        testSum.calculate(new BufferedReader(new StringReader("PUSH 9e307")));
        testSum.calculate(new BufferedReader(new StringReader("PUSH 9e307")));
        assertThrows(NumberOverflowException.class,
                () -> testSum.calculate(new BufferedReader(new StringReader("+"))));
    }
}