package calcoperations;

import calcexceptions.*;
import org.junit.jupiter.api.Test;
import stackcalc.StackCalc;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

class DefineOperationTest {
    @Test
    void testDoOperation() throws Exception {
        StackCalc testDefine = new StackCalc();
        testDefine.calculate(new BufferedReader(new StringReader("DEFINE val 100")));
        assertEquals(100, testDefine.getConstant("val"));
        testDefine.calculate(new BufferedReader(new StringReader("DEFINE BigVal 1e100")));
        assertEquals(1e100, testDefine.getConstant("BigVal"));
        testDefine.calculate(new BufferedReader(new StringReader("DEFINE NegVal -123\nDEFINE BigNegVal -1.2e123")));
        assertEquals(-123, testDefine.getConstant("NegVal"));
        assertEquals(-1.2e123, testDefine.getConstant("BigNegVal"));

        assertThrows(BadNameForDefineException.class,
                                            () -> testDefine.calculate(new BufferedReader(new StringReader("DEFINE 10e+1 101"))));

        assertThrows(ManyArgumentsException.class,
                                            () -> testDefine.calculate(new BufferedReader(new StringReader("DEFINE Many 123 POP"))));

        assertThrows(FewArgumentsException.class,
                                            () -> testDefine.calculate(new BufferedReader(new StringReader("DEFINE Few"))));

        assertThrows(BadArgumentFormatException.class,
                                            () -> testDefine.calculate(new BufferedReader(new StringReader("DEFINE BadValue 123wqw"))));

        assertThrows(NumberOverflowException.class,
                                            () -> testDefine.calculate(new BufferedReader(new StringReader("DEFINE Inf Infinity"))));

        assertThrows(NotANumberException.class,
                                            () -> testDefine.calculate(new BufferedReader(new StringReader("DEFINE NotANumber NaN"))));

    }
}