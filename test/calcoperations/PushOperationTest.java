package calcoperations;

import calcexceptions.*;
import org.junit.jupiter.api.Test;
import stackcalc.StackCalc;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

class PushOperationTest {
    @Test
    void testDoOperation() throws Exception {
        StackCalc testPush = new StackCalc();
        testPush.calculate(new BufferedReader(new StringReader("PUSH 1200")));
        assertEquals(1200, testPush.getTopStack());
        testPush.calculate(new BufferedReader(new StringReader("PUSH -1e100")));
        assertEquals(-1e100, testPush.getTopStack());

        assertThrows(ManyArgumentsException.class,
                () -> testPush.calculate(new BufferedReader(new StringReader("PUSH 1e1 Many"))));

        assertThrows(FewArgumentsException.class,
                () -> testPush.calculate(new BufferedReader(new StringReader("PUSH"))));

        assertThrows(BadArgumentFormatException.class,
                () -> testPush.calculate(new BufferedReader(new StringReader("PUSH 123wqw"))));

        assertThrows(NumberOverflowException.class,
                () -> testPush.calculate(new BufferedReader(new StringReader("PUSH Infinity"))));

        assertThrows(NotANumberException.class,
                () -> testPush.calculate(new BufferedReader(new StringReader("PUSH NaN"))));

    }

}