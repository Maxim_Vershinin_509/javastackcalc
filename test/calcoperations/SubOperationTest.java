package calcoperations;

import calcexceptions.FewStackSizeException;
import calcexceptions.ManyArgumentsException;
import calcexceptions.NumberOverflowException;
import org.junit.jupiter.api.Test;
import stackcalc.StackCalc;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

class SubOperationTest {
    @Test
    void testSubOperation() throws Exception {
        StackCalc testSub = new StackCalc();
        assertThrows(ManyArgumentsException.class,
                () -> testSub.calculate(new BufferedReader(new StringReader("- asdqweqwe\nsadqw"))));

        assertThrows(FewStackSizeException.class,
                () -> testSub.calculate(new BufferedReader(new StringReader("-\nPRINT"))));

        testSub.calculate(new BufferedReader(new StringReader("PUSH 100")));
        assertThrows(FewStackSizeException.class,
                () -> testSub.calculate(new BufferedReader(new StringReader("-\n# qwerty"))));

        testSub.calculate(new BufferedReader(new StringReader("PUSH 10\n-")));
        assertEquals(90, testSub.getTopStack());
        testSub.calculate(new BufferedReader(new StringReader("PUSH 9e307")));
        testSub.calculate(new BufferedReader(new StringReader("PUSH -9e307")));
        assertThrows(NumberOverflowException.class,
                () -> testSub.calculate(new BufferedReader(new StringReader("-"))));
    }
}