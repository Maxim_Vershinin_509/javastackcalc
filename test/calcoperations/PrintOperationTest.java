package calcoperations;

import calcexceptions.FewStackSizeException;
import calcexceptions.ManyArgumentsException;
import org.junit.jupiter.api.Test;
import stackcalc.StackCalc;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

class PrintOperationTest {
    @Test
    void testPrintOperation() throws Exception {
        StackCalc testPrint = new StackCalc();

        assertThrows(ManyArgumentsException.class,
                                            () -> testPrint.calculate(new BufferedReader(new StringReader("PRINT Many"))));

        assertThrows(FewStackSizeException.class,
                                            () -> testPrint.calculate(new BufferedReader(new StringReader("PRINT"))));
    }

}