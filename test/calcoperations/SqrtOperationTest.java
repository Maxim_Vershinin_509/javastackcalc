package calcoperations;

import calcexceptions.FewStackSizeException;
import calcexceptions.ManyArgumentsException;
import calcexceptions.ValueLessZeroException;
import org.junit.jupiter.api.Test;
import stackcalc.StackCalc;

import java.io.BufferedReader;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

class SqrtOperationTest {
    @Test
    void testSqrtOperation() throws Exception {
        StackCalc testSqrt = new StackCalc();
        assertThrows(ManyArgumentsException.class,
                () -> testSqrt.calculate(new BufferedReader(new StringReader("SQRT asdqweqwe\nsadqw"))));

        assertThrows(FewStackSizeException.class,
                () -> testSqrt.calculate(new BufferedReader(new StringReader("SQRT\nPRINT"))));

        testSqrt.calculate(new BufferedReader(new StringReader("PUSH 100")));
        testSqrt.calculate(new BufferedReader(new StringReader("SQRT")));
        assertEquals(10, testSqrt.getTopStack());

        testSqrt.calculate(new BufferedReader(new StringReader("PUSH -1.1")));

        assertThrows(ValueLessZeroException.class,
                                            () -> testSqrt.calculate(new BufferedReader(new StringReader("SQRT"))));
    }
}