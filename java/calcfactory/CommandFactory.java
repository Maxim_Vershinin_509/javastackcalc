package calcfactory;

import calcexceptions.IncorrectOperationException;
import calcexceptions.NotFoundConfigFactoryFileException;
import calcoperations.Operation;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class CommandFactory {

    private final Properties factoryProperties = new Properties();

    private Map<String, Class<?>> factoryMap = new HashMap<>();

    public CommandFactory() throws NotFoundConfigFactoryFileException {
        try {
            factoryProperties.load(new FileInputStream("./resources/factoryConfig.properties"));
        } catch (IOException e ) {
            throw new NotFoundConfigFactoryFileException(getClass().getSimpleName() +
                                        ": not found factory configuration file");
        }
    }

    public Operation createCommand(String command) throws Exception {
        Operation op;
        try {
            if (null == factoryMap.get(command)) {
                factoryMap.put(command,Class.forName(factoryProperties.getProperty(command)));
            }
            op = (Operation) factoryMap.get(command).getConstructor().newInstance();
        } catch (Exception e) {
            throw new IncorrectOperationException(getClass().getSimpleName(), command);
        }
        return op;
    }

}
