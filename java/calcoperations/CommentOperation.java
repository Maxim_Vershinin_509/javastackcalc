package calcoperations;

import stackcalc.StackCalc;

import java.util.List;
import java.util.logging.Logger;

public class CommentOperation implements Operation {
    private static final Logger LOGGER = Logger.getLogger(CommentOperation.class.getName());

    @Override
    public void doOperation(StackCalc.StackAndConstants stackAndConstants, List<String> optionsList) {
        LOGGER.fine("Stating " + getClass().getSimpleName());
        System.out.println(optionsList.toString());
        LOGGER.fine("Finishing " + getClass().getSimpleName());
    }
}
