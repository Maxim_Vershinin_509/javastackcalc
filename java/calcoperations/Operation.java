package calcoperations;

import stackcalc.StackCalc;

import java.util.List;

public interface Operation {
    void doOperation(StackCalc.StackAndConstants stackAndConstants, List<String> optionsList) throws Exception;

}
