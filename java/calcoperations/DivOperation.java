package calcoperations;

import calcexceptions.DivisionByZeroException;


public class DivOperation extends BinaryOperation {

    @Override
    Double doBinOp(Double numb1, Double numb2) throws DivisionByZeroException {
        if (numb1.equals(0.0)) {
            throw new DivisionByZeroException(getClass().getSimpleName() + ": division by zero ");
        }
        return numb2/numb1;
    }
}
