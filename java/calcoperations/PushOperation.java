package calcoperations;

import calcexceptions.*;
import stackcalc.StackCalc;

import java.util.List;
import java.util.logging.Logger;

public class PushOperation implements Operation {
    static private final Logger LOGGER = Logger.getLogger(PushOperation.class.getName());

    @Override
    public void doOperation(StackCalc.StackAndConstants stackAndConstants, List<String> optionsList) throws Exception {
        LOGGER.fine("Starting " + getClass().getSimpleName());

        if (optionsList.size() > 1) {
            throw new ManyArgumentsException(getClass().getSimpleName(), optionsList.size(), 1);
        }

        if (optionsList.size() < 1) {
            throw new FewArgumentsException(getClass().getSimpleName(), optionsList.size(), 1);
        }

        Double pushData = stackAndConstants.getConstant(optionsList.get(0));

        if (null != pushData) {
            stackAndConstants.pushStack(pushData);
        } else {
            try {
                pushData = Double.parseDouble(optionsList.get(0));

                if (pushData.isInfinite()) {
                    throw new NumberOverflowException(getClass().getSimpleName() + ": push value overflowed error");
                }

                if (pushData.isNaN()) {
                    throw new NotANumberException(getClass().getSimpleName() + ": push value is NaN error");
                }

                stackAndConstants.pushStack(pushData);
            } catch (NumberFormatException e) {
                throw new BadArgumentFormatException(getClass().getSimpleName(), optionsList.get(0));
            }
        }

        LOGGER.fine("Finishing " + getClass().getSimpleName() + ": was pushed: \"" + pushData + "\"");
    }
}
