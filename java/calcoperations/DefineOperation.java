package calcoperations;

import calcexceptions.*;
import stackcalc.StackCalc;

import java.util.List;
import java.util.logging.Logger;

public class DefineOperation implements Operation {
    private static final Logger LOGGER = Logger.getLogger(DefineOperation.class.getName());

    @Override
    public void doOperation(StackCalc.StackAndConstants stackAndConstants, List<String> optionsList) throws Exception {
        LOGGER.fine("Starting " + getClass().getSimpleName());

        if (optionsList.size() > 2) {
            throw new ManyArgumentsException(getClass().getSimpleName(), optionsList.size(), 2);
        }

        if (optionsList.size() < 2) {
            throw new FewArgumentsException(getClass().getSimpleName(), optionsList.size(), 2);
        }

        String def = optionsList.get(0);
        boolean isCorrectDefine;

        try {
            Double.parseDouble(def);
            isCorrectDefine = false;
        } catch (NumberFormatException e) {
            isCorrectDefine = true;
        }

        if (!isCorrectDefine) {
            throw new BadNameForDefineException(getClass().getSimpleName(), def);
        }

        Double value;
        try {
            value = Double.parseDouble(optionsList.get(1));
        } catch (NumberFormatException e) {
            throw new BadArgumentFormatException(getClass().getSimpleName(), optionsList.get(1));
        }

        if (value.isInfinite()) {
            throw new NumberOverflowException(getClass().getSimpleName() + ": constant overflowed error");
        }

        if (value.isNaN()) {
            throw new NotANumberException(getClass().getSimpleName() + ": constant is NaN error");
        }

        stackAndConstants.putConstant(def, value);

        LOGGER.fine("Finishing " + getClass().getSimpleName() +
                ": constant is \"" + def + "\", " +
                "value is \"" + value + "\"");
    }
}
