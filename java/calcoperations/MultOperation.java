package calcoperations;

public class MultOperation extends BinaryOperation{
    @Override
    Double doBinOp(Double numb1, Double numb2) {
        return numb2 * numb1;
    }
}
