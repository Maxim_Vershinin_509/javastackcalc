package calcoperations;

import calcexceptions.*;
import stackcalc.StackCalc;

import java.util.List;
import java.util.logging.Logger;

public class SqrtOperation extends UnaryOperation {
    static private final Logger LOGGER = Logger.getLogger(SqrtOperation.class.getName());

    @Override
    public void doOperation(StackCalc.StackAndConstants stackAndConstants, List<String> optionsList) throws Exception {
        LOGGER.fine("Starting " + getClass().getSimpleName());

        checkArguments(stackAndConstants, optionsList);

        Double numb = stackAndConstants.getTopStack();

        if (numb < 0) {
            throw new ValueLessZeroException(getClass().getSimpleName(), numb);
        }

        stackAndConstants.popStack();
        stackAndConstants.pushStack(Math.sqrt(numb));

        LOGGER.fine("Finishing " + getClass().getSimpleName() + ": result is \"" + Math.sqrt(numb) + "\"");
    }

}
