package calcoperations;

import calcexceptions.*;
import stackcalc.StackCalc;

import java.util.List;
import java.util.logging.Logger;

public abstract class BinaryOperation implements Operation {
    private static final Logger LOGGER = Logger.getLogger(BinaryOperation.class.getName());

    public void doOperation(StackCalc.StackAndConstants stackAndConstants, List<String> optionsList) throws Exception {
        LOGGER.fine("Starting " + this.getClass().getSimpleName());

        if (optionsList.size() > 0) {
            throw new ManyArgumentsException(getClass().getSimpleName(), optionsList.size(), 0);
        }

        if (stackAndConstants.getStackSize() < 2) {
            throw new FewStackSizeException(getClass().getSimpleName(), stackAndConstants.getStackSize(), 2);
        }

        Double numb1 = stackAndConstants.popStack();
        Double numb2 = stackAndConstants.getTopStack();

        stackAndConstants.pushStack(numb1);
        Double res = this.doBinOp(numb1, numb2);

        if (res.isInfinite()) {
            throw new NumberOverflowException(getClass().getSimpleName() + ": result overflowed error");
        }

        stackAndConstants.popStack();
        stackAndConstants.popStack();
        stackAndConstants.pushStack(res);

        LOGGER.fine("Finishing " + getClass().getSimpleName() + ": result is \"" + res + "\"");
    }

    abstract Double doBinOp(Double numb1, Double numb2) throws Exception;
}
