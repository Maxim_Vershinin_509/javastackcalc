package calcoperations;

import calcexceptions.FewStackSizeException;
import calcexceptions.ManyArgumentsException;
import stackcalc.StackCalc;

import java.util.List;

public abstract class UnaryOperation implements Operation {
    void checkArguments(StackCalc.StackAndConstants stackAndConstants, List<String> optionsList) throws Exception {
        if (optionsList.size() > 0) {
            throw new ManyArgumentsException(getClass().getSimpleName(), optionsList.size(), 0);
        }

        if (0 == stackAndConstants.getStackSize()) {
            throw new FewStackSizeException(getClass().getSimpleName() + ": stack is empty");
        }
    }
}
