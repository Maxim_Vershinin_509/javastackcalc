package calcoperations;

import stackcalc.StackCalc;

import java.util.List;
import java.util.logging.Logger;

public class PopOperation extends UnaryOperation {
    static private final Logger LOGGER = Logger.getLogger(PopOperation.class.getName());

    @Override
    public void doOperation(StackCalc.StackAndConstants stackAndConstants, List<String> optionsList) throws Exception {
        LOGGER.fine("Starting " + getClass().getSimpleName());

        checkArguments(stackAndConstants, optionsList);

        LOGGER.fine("Finishing " + getClass().getSimpleName() + ": was pulled \"" + stackAndConstants.popStack() + "\"");
    }
    
}
