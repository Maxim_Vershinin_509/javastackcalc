package calcoperations;
public
class SumOperation extends BinaryOperation {
    @Override
    Double doBinOp(Double numb1, Double numb2) {
        return numb2 + numb1;
    }
}
