package calcoperations;

import stackcalc.StackCalc;

import java.util.List;
import java.util.logging.Logger;

public class PrintOperation extends UnaryOperation {
    static private final Logger LOGGER = Logger.getLogger(PrintOperation.class.getName());

    @Override
    public void doOperation(StackCalc.StackAndConstants stackAndConstants, List<String> optionsList) throws Exception {
        LOGGER.fine("Starting " + getClass().getSimpleName());

        checkArguments(stackAndConstants, optionsList);

        System.out.println(stackAndConstants.getTopStack());

        LOGGER.fine("Finishing " + getClass().getSimpleName() +
                ": was printed: \"" + stackAndConstants.getTopStack() + "\"");
    }

}
