package calcoperations;

public class SubOperation extends BinaryOperation {
    @Override
    Double doBinOp(Double numb1, Double numb2) {
        return numb2 - numb1;
    }
}
