package stackcalc;

import calcfactory.CommandFactory;
import calcoperations.Operation;

import java.io.BufferedReader;
import java.util.*;
import java.util.logging.Logger;


public class StackCalc {
    static public class StackAndConstants {
        private Stack<Double> stack = new Stack<>();
        private Map<String, Double> constants = new HashMap<>();

        public int getStackSize() { return stack.size(); }

        public Double getTopStack() { return stack.peek(); }

        public Double popStack() { return stack.pop(); }

        public void pushStack(Double value) { stack.push(value); }

        public void putConstant(String key, Double value) {
            constants.put(key, value);
        }

        public Double getConstant(String key) { return constants.get(key); }

    }

    private static Logger LOGGER = Logger.getLogger(StackCalc.class.getName());

    private CommandFactory commandFactory;
    private StackAndConstants stackAndConstants = new StackAndConstants();

    private List<String> parseStr(String str) {
        LinkedList<String> listStr = new LinkedList<>();
        StringBuilder bufStr = new StringBuilder();

        if (null == str) return listStr;

        for (char s: str.toCharArray()) {
            if (' ' != s) {
                bufStr.append(s);
            } else {
                if (0 != bufStr.length()) {
                    listStr.addLast(bufStr.toString());
                    bufStr = new StringBuilder();
                }
            }
        }
        if (0 != bufStr.length()) {
            listStr.addLast(bufStr.toString());
        }

        return listStr;
    }

    private void doOperation(String str) throws Exception {
        LOGGER.fine("Start parsing string");
        List<String> optionsList = parseStr(str);

        if (optionsList.isEmpty()) {
            LOGGER.fine("String is empty");
            return;
        }

        String command = optionsList.get(0);

        optionsList.remove(0);

        LOGGER.fine("Validation of the entered data " +
                            "for compliance with the operation");

        Operation op = commandFactory.createCommand(command);
        LOGGER.fine("Operation call");
        op.doOperation(stackAndConstants, optionsList);
    }

    private void calculateWithFile(BufferedReader reader) throws Exception {
        String str;
        while (null != (str = reader.readLine())) {
            if (0 != str.length()) {
                doOperation(str);
            }
        }
    }

    private void calculateWithoutFile() throws Exception {
        while (true) {
            Scanner in = new Scanner(System.in);
            String str;
            str = in.nextLine();
            if (str.equals(("EXIT")))  {
                LOGGER.fine("\"EXIT\" was entered");
                break;
            }
            doOperation(str);
        }
    }

    public StackCalc() throws Exception {
        super();
        commandFactory = new CommandFactory();
    }

    public void calculate(BufferedReader reader) throws Exception {
        if (null == reader) {
            calculateWithoutFile();
        } else {
            calculateWithFile(reader);
        }
    }

    public Double getTopStack() {
        return stackAndConstants.getTopStack();
    }

    public Double getConstant(String define) { return stackAndConstants.getConstant(define); }
}
