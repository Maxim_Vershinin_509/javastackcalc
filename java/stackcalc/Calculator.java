package stackcalc;

import java.io.*;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Calculator {
    private static final Logger LOGGER = Logger.getLogger(Calculator.class.getName());

    public static void main(String[] args) {

        BufferedReader reader = null;
        try {
            LogManager.getLogManager().
                    readConfiguration(Calculator.class.getResourceAsStream("/logging.properties"));
        } catch (Exception er) {
            System.err.println("Not found logger configuration file");
        }

        StackCalc stackCalc;

        try {
            stackCalc = new StackCalc();
        } catch (Exception er) {
            LOGGER.severe(er.getMessage());
            return;
        }

        try {
            if (0 < args.length) {
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(args[0])));
                LOGGER.fine("File was opened");
            } else {
                LOGGER.fine("File's name was't entered, starting work with console");
            }
        } catch (FileNotFoundException e) {
            LOGGER.fine(e.getMessage() + ", starting work with console");
        } finally {
            while (true) {
                try {
                    stackCalc.calculate(reader);
                    break;
                } catch (Exception e) {
                    LOGGER.severe(e.getMessage());
                }
            }

            if (null != reader) {
                try {
                    reader.close();
                } catch (IOException e) {
                    LOGGER.severe(e.getMessage());
                }
            }
        }
    }
}
