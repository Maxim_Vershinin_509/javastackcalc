package calcexceptions;

public class ManyArgumentsException extends Exception {
    private int actualNumberOfArguments;
    private int expectedNumberOfArguments;
    private String className = null;

    @Override
    public String getMessage() {
        if (null == className) return super.getMessage();
        return className + ": many arguments error: actual - "
                + actualNumberOfArguments + ", expected - " + expectedNumberOfArguments;
    }

    public ManyArgumentsException(String className, int actualNumberOfArguments, int expectedNumberOfArguments) {
        this.className = className;
        this.actualNumberOfArguments = actualNumberOfArguments;
        this.expectedNumberOfArguments = expectedNumberOfArguments;
    }

    public ManyArgumentsException(String message) {
        super(message);
    }
}
