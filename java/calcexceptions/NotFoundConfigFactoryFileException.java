package calcexceptions;

public class NotFoundConfigFactoryFileException extends Exception {
    public NotFoundConfigFactoryFileException(String message) { super(message); }

}

