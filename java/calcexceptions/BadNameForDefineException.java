package calcexceptions;

public class BadNameForDefineException extends Exception {
    private String name;
    private String className;

    @Override
    public String getMessage() {
        if (null == className) return super.getMessage();
        return className + ": \"" + name + "\" is bad name for define";
    }

    public BadNameForDefineException(String className, String name) {
        this.className = className;
        this.name = name;
    }

    public BadNameForDefineException(String message) {
        super(message);
    }
}
