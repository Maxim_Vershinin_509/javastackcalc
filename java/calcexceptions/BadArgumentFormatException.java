package calcexceptions;

public class BadArgumentFormatException extends Exception {
    private String argument;
    private String className;

    @Override
    public String getMessage() {
        if (null == className) return super.getMessage();
        return className + ": \"" + argument + "\" bad argument format for number";
    }

    public BadArgumentFormatException(String className, String argument) {
        this.className = className;
        this.argument = argument;
    }


    public BadArgumentFormatException(String message) {
        super(message);
    }
}
