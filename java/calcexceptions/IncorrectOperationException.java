package calcexceptions;

public class IncorrectOperationException extends Exception {
    private String operationName;
    private String className;

    @Override
    public String getMessage() {
        if (null == className) return super.getMessage();
        return className + ": " + operationName + " is incorrect operation";
    }

    public IncorrectOperationException(String className, String  operationName) {
        this.className = className;
        this.operationName = operationName;
    }

    public IncorrectOperationException(String message) {
        super(message);
    }

}
