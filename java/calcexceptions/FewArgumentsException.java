package calcexceptions;

public class FewArgumentsException extends Exception {
    private int actualNumberOfArguments;
    private int expectedNumberOfArguments;
    private String className = null;

    @Override
    public String getMessage() {
        if (null == className) return super.getMessage();
        return className + ": few arguments error: actual - "
                + actualNumberOfArguments + ", expected - " + expectedNumberOfArguments;
    }

    public FewArgumentsException(String className, int actualNumberOfArguments, int expectedNumberOfArguments) {
        this.className = className;
        this.actualNumberOfArguments = actualNumberOfArguments;
        this.expectedNumberOfArguments = expectedNumberOfArguments;
    }

    public FewArgumentsException(String message) {
        super(message);
    }
}
