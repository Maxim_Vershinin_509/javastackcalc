package calcexceptions;

public class ValueLessZeroException extends Exception {
    private double numberLessZero;
    private String className;

    @Override
    public String getMessage() {
        if (null == className) return super.getMessage();
        return className + ": " + numberLessZero + " less than zero";
    }

    public ValueLessZeroException(String className, double numberLessZero) {
        this.className = className;
        this.numberLessZero = numberLessZero;
    }

    public ValueLessZeroException(String s) {
        super(s);
    }

}
