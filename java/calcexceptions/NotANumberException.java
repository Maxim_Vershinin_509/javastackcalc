package calcexceptions;

public class NotANumberException extends Exception {
    public NotANumberException(String s) {
        super(s);
    }

}
