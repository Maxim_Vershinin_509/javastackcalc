package calcexceptions;

public class FewStackSizeException extends Exception {
    private int actualStackSize;
    private int expectedStackSize;
    private String className = null;

    public FewStackSizeException(String message) {
        super(message);
    }

    public FewStackSizeException(String className, int actualStackSize, int expectedStackSize) {
        this.className = className;
        this.actualStackSize = actualStackSize;
        this.expectedStackSize = expectedStackSize;
    }

    @Override
    public String getMessage() {
        if (null == className) return super.getMessage();
        return className + ": few stack size: actual - " + actualStackSize + ", expected - " + expectedStackSize;
    }
}
